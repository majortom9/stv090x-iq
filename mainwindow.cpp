#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    qRegisterMetaType<double>("double");
    qRegisterMetaType<QVector<double> >("QVector<double>");

    connect(&myiq, SIGNAL(signaldraw(QVector<double>, QVector<double>)), this, SLOT(qwt_draw(QVector<double>, QVector<double>)));

    curve_1 = new QwtPlotCurve("Curve 1");
	curve_1->setStyle(QwtPlotCurve::NoCurve);
	curve_1->attach(ui->qwtPlot);
	curve_2 = new QwtPlotCurve("Curve 2");
	curve_2->setStyle(QwtPlotCurve::NoCurve);
	curve_2->attach(ui->qwtPlot);
	curve_3 = new QwtPlotCurve("Curve 3");
	curve_3->setStyle(QwtPlotCurve::NoCurve);
	curve_3->attach(ui->qwtPlot);

    scatter_symbol_1 = new QwtSymbol;
	scatter_symbol_1->setStyle(QwtSymbol::Ellipse);
	scatter_symbol_1->setSize(5,5);
    scatter_symbol_1->setPen(QColor(255, 0, 0));
    scatter_symbol_1->setBrush(QColor(255, 0, 0));
    curve_1->setSymbol(scatter_symbol_1);
	scatter_symbol_2 = new QwtSymbol;
	scatter_symbol_2->setStyle(QwtSymbol::Ellipse);
    scatter_symbol_2->setSize(4,4);
    scatter_symbol_2->setPen(QColor(200, 0, 0));
    scatter_symbol_2->setBrush(QColor(200, 0, 0));
    curve_2->setSymbol(scatter_symbol_2);
	scatter_symbol_3 = new QwtSymbol;
	scatter_symbol_3->setStyle(QwtSymbol::Ellipse);
	scatter_symbol_3->setSize(5,5);
    scatter_symbol_3->setPen(QColor(150, 0, 0));
    scatter_symbol_3->setBrush(QColor(150, 0, 0));
    curve_3->setSymbol(scatter_symbol_3);

	ui->qwtPlot->setAxisTitle(QwtPlot::xBottom, "Real");
	ui->qwtPlot->setAxisTitle(QwtPlot::yLeft, "Imaginary");
	ui->qwtPlot->setAxisScale(QwtPlot::xBottom, -64, 64);
	ui->qwtPlot->setAxisScale(QwtPlot::yLeft, -64, 64);
	ui->qwtPlot->enableAxis(QwtPlot::xBottom ,0);
	ui->qwtPlot->enableAxis(QwtPlot::yLeft ,0);

	scaleX = new QwtPlotScaleItem();
	scaleX->setAlignment(QwtScaleDraw::BottomScale);
	scaleX->setScaleDiv((new QwtLinearScaleEngine())->divideScale(-64, 64, 10, 5));
	scaleX->attach(ui->qwtPlot);
	scaleY = new QwtPlotScaleItem();
	scaleY->setAlignment(QwtScaleDraw::LeftScale);
	scaleY->setScaleDiv((new QwtLinearScaleEngine())->divideScale(-64, 64, 10, 5));
	scaleY->attach(ui->qwtPlot);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::qwt_draw(QVector<double> x, QVector<double> y)
{
	QVector<double> x_1;
	QVector<double> y_1;
	QVector<double> xy_1;	
	QVector<double> x_2;
	QVector<double> y_2;
	QVector<double> xy_2;
	QVector<double> x_3;
	QVector<double> y_3;
	QVector<double> xy_3;

//	cout << "qwt_draw()" << endl;
	
	for (int i = 0; i < x.size(); i++) {
		if (!xy_1.contains(x[i]*256 + y[i])) {
			x_1.append(x[i]);
			y_1.append(y[i]);
			xy_1.append(x[i]*256 + y[i]);
			continue;
		}
		if (!xy_2.contains(x[i]*256 + y[i])) {
			x_2.append(x[i]);
			y_2.append(y[i]);
			xy_2.append(x[i]*256 + y[i]);
			continue;
		}
		if (!xy_3.contains(x[i]*256 + y[i])) {
			x_3.append(x[i]);
			y_3.append(y[i]);
			xy_3.append(x[i]*256 + y[i]);
			continue;
		}
	}
	
	curve_1->setSamples(x_1, y_1);
	curve_2->setSamples(x_2, y_2);
	curve_3->setSamples(x_3, y_3);
    ui->qwtPlot->replot();
	myiq.setup(ui->adapterBox->currentText().toInt(), ui->loopBox->isChecked(), ui->persistenceScrollBar->value());
}

void MainWindow::on_updateButton_clicked()
{
    cout << "on_pushButton_clicked()" << endl;
	myiq.setup(ui->adapterBox->currentText().toInt(), ui->loopBox->isChecked(), ui->persistenceScrollBar->value());
    myiq.start();
}

void MainWindow::on_actionExit_triggered()
{
    myiq.closeadapter();
    myiq.quit();
    myiq.wait();
    exit(1);
}
