#-------------------------------------------------
#
# Project created by QtCreator 2012-07-17T17:21:10
#
#-------------------------------------------------

//QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
TARGET = stv090x-iq
TEMPLATE = app

SOURCES += mainwindow.cpp
SOURCES += stv090x-iq.cpp
SOURCES += main.cpp

HEADERS  += mainwindow.h
HEADERS  += stv090x-iq.h

FORMS    += mainwindow.ui

INCLUDEPATH += /usr/local/qwt-6.1.3-svn/include
INCLUDEPATH += /usr/include/qwt
LIBS += -Wl,-rpath,/usr/local/qwt-6.1.3-svn/lib -L /usr/local/qwt-6.1.3-svn/lib -l:libqwt.so

#LIBS += -l qwt

